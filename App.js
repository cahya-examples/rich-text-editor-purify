import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import RichTextEditor from 'react-rte';
import dompurify from 'dompurify';

class App extends Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.state = {
          value: RichTextEditor.createEmptyValue(), //rich text editornya nyimpen data (apa yang diketik di editor) disini
          dangerousInnerHtml: '',
        }
    }
  
    onChange(value) {
      this.setState({value});
    };

    changeDangerous(value) {
      const val = dompurify.sanitize(value);
      this.setState({dangerousInnerHtml: val})
      console.log(val);
    };

    getFromAPI() {
      const token = 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InBhbmRlLmtldHV0NzEiLCJ1c2VyX2lkIjoxMSwiZW1haWwiOiIiLCJleHAiOjE1NjI1NTk3ODJ9.vbI4blN0JhqKNI1t_XjTDdcC0v5EHE92kC-lsB-qssQ';
      axios.get('http://127.0.0.1:8000/pmb-api/post/3/', {  // ini ngambil post yg id 3, hardcoded
        headers: {
          Authorization: token
        }
      }).then( (response) => {
        const val = dompurify(response.data.content); // string HTML dari backend di purify
        this.changeDangerous(val);
      });
    }

    sendToAPI() {
      const val = dompurify(this.state.value.toString('html')); // hal yang diketik di convert ke HTML string, trus di purify
      const token = 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InBhbmRlLmtldHV0NzEiLCJ1c2VyX2lkIjoxMSwiZW1haWwiOiIiLCJleHAiOjE1NjI1NTk3ODJ9.vbI4blN0JhqKNI1t_XjTDdcC0v5EHE92kC-lsB-qssQ';
      axios.post(`http://127.0.0.1:8000/pmb-api/post/`, {
        title: 'Hai',
        summary: 'Summary',
        content: val,
        post_type: 1
      }, {
        headers: {
          Authorization: token
        }
      });
    }
  
    render () {
      return (
        <div>
          <RichTextEditor
            value={this.state.value}  // RichTextEditor simpen yg diketik disini
            onChange={this.onChange}  // RichTextEditor manggil fungsi ini setiap perubahan ketikan
          />
          <div dangerouslySetInnerHTML={ {__html: this.state.dangerousInnerHtml} }></div>
          <button onClick={ () => this.getFromAPI() }>Get from API</button>
          <button onClick={ () => this.sendToAPI() }>Send Dangerous To API</button>
        </div>
      );
    }
}

export default App;
